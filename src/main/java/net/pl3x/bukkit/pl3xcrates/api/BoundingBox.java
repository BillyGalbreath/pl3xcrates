package net.pl3x.bukkit.pl3xcrates.api;

import org.bukkit.util.Vector;

public interface BoundingBox {
    Vector getMin();

    Vector getMax();
}
