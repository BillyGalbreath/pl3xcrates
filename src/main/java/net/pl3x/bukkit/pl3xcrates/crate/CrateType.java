package net.pl3x.bukkit.pl3xcrates.crate;

public enum CrateType {
    NORMAL,
    ROULETTE,
    ROULETTE_FANCY,
    SLIDE,
    SLIDE_FANCY
}
